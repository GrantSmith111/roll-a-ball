using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using TMPro;

public class PlayerController : MonoBehaviour
{
    public float speed = 0;
    public TextMeshProUGUI countText;
    public GameObject winTextObject;

    private Rigidbody rb;
    private int count;

    private float movementX;
    private float movementY;

    public InputAction fireAction;

    private bool onGround = true;
    private bool canDash = false;
     Color lerpedColor = Color.blue;
    private bool willBounce = false;
    


    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        count = 0;

        SetCountText();
        winTextObject.SetActive(false);

        fireAction.Enable();

    }

    void OnCollisionEnter( Collision coll ) 
    {
      onGround = true;
      canDash = false;
      if(willBounce == true) 
      {
          rb.AddForce(new Vector3(0f, 500f, 0f));
          willBounce = false;
      }
    }


    private void OnJump()
    {
        if(onGround == true) {
            if(canDash == false) {
                rb.AddForce(new Vector3(0f, 300f, 0f));
                onGround = false;
                canDash = true;
            }  
        }
        else if(canDash == true)
        {
            rb.AddForce(new Vector3(movementX, 0f, movementY) * 1000f);
            canDash = false;
        }
    }

    private void OnFire() 
    {
        if(onGround == false) 
        {
            rb.AddForce(new Vector3(0f, -1000f, 0f));
            willBounce = true;
        }
    }

    private void Update() 
    {
        lerpedColor = Color.Lerp(Color.blue, Color.red, GetComponent<Rigidbody>().velocity.magnitude / 30);
        gameObject.GetComponent<Renderer>().material.color = lerpedColor;


    }

    private void OnMove(InputValue movementValue)
    {
        Vector2 movementVector = movementValue.Get<Vector2>();

        movementX = movementVector.x;
        movementY = movementVector.y;
    }

    void SetCountText() 
    {
        countText.text = "Count: " + count.ToString();
        if(count >= 12) 
        {
            winTextObject.SetActive(true);
        }
    }

    private void FixedUpdate()
    {
        Vector3 movement = new Vector3(movementX, 0.0f, movementY);

        rb.AddForce(movement * speed);
    }

    void OnTriggerEnter(Collider other) 
    {
        if (other.gameObject.CompareTag("PickUp")) 
        {
            other.gameObject.SetActive(false);
            count = count + 1;

            SetCountText();
        }
        
    }

}
